const cells   = document.querySelectorAll(".cell")
let checkTurn = true

const PLAYER_X = "X"
const PLAYER_O = "O"

const COMBINATIONS = [
    [0,1,2],
    [3,4,5],
    [6,7,8],
    [0,3,6],
    [1,4,7],
    [2,5,8],
    [0,4,8],
    [2,4,6]
];

document.addEventListener("click", (event)=> {
    if(event.target.matches(".cell")){
        movement(event.target.id)
    }
})

function movement(id){
    const cell = document.getElementById(id)
    turn = checkTurn ? PLAYER_X : PLAYER_O
    cell.textContent = turn
    cell.classList.add(turn)
    checkWin(turn)
}

function checkWin(turn){
    const winner = COMBINATIONS.some((comb) => {
        return comb.every((index) => {
            return cells[index].classList.contains(turn);
        })
    })

    if(winner){
        endGame(turn)
    }
    else if(checkDraw()){
        endGame()
    }
    else{
        checkTurn = !checkTurn
    }
}

function checkDraw(){
    let x = 0
    let o = 0

    for(index in cells){
        if(!isNaN(index)){
            if(cells[index].classList.contains(PLAYER_X)){
                x++
            }

            if(cells[index].classList.contains(PLAYER_O)){
                o++
            }
        }
    }

    return x + o == 9 ? true : false
}

function endGame(winner = null){
    let msg = "Partida Finalizada "
    let win = ""

    if(winner){
        win = winner
        console.log(msg + win + " ganhou!")
    } else{
        win = "Empate"
        console.log(msg + win)
    }

    return {msg, win}
}
